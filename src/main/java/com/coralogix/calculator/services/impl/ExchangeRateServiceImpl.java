package com.coralogix.calculator.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.coralogix.calculator.dao.ExchangeRateDao;
import com.coralogix.calculator.dto.Fixer;
import com.coralogix.calculator.model.ExchangeRate;
import com.coralogix.calculator.services.ExchangeRateService;

import reactor.core.publisher.Mono;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	@Autowired
	ExchangeRateDao exchangeRateDao;

	@Autowired
	RestTemplate restTemplateClient;

	@Override
	public Mono<ExchangeRate> getAllExchangeRate(String originCurrency, String finalCurrency) {
		ExchangeRate consulta = new ExchangeRate();
		ExchangeRate response = new ExchangeRate();
		consulta.setFinalCurrency(finalCurrency);
		consulta.setOriginCurrency(originCurrency);
		Example<ExchangeRate> eRate = Example.of(consulta);

		List<ExchangeRate> lists = exchangeRateDao.findAll(eRate);

		if (lists.isEmpty()) {
			Fixer request = restTemplateClient.getForObject(
					"http://localhost:3000/fixer/latest?base=" + originCurrency + "&symbols=" + finalCurrency,
					Fixer.class);

			if (Objects.nonNull(request)) {
				ExchangeRate ex = new ExchangeRate();
				ex.setFinalCurrency(finalCurrency);
				ex.setOriginCurrency(originCurrency);
				ex.setDate(request.getDate());
				ex.setValue(request.getRates().get(finalCurrency).toString());
				response = exchangeRateDao.save(ex);
			}
		} else {
			response.setDate(lists.get(0).getDate());
			response.setFinalCurrency(lists.get(0).getFinalCurrency());
			response.setId(lists.get(0).getId());
			response.setOriginCurrency(lists.get(0).getOriginCurrency());
			response.setValue(lists.get(0).getValue());
			response.setFinalCurrency(lists.get(0).getFinalCurrency());
		}
		return Mono.just(response);
	}

	@Override
	public Integer getMatriz(Integer uservalue) {
		List<Integer> entero = new ArrayList<>(); 
//		for(int i=1;i<=10;i++) {
//			for(int j=1;j<=10;j++) {
//				[][]Integer matriz=new Integer[][];
//				entero.add((uservalue*i)+ matriz[i-1][j])
//			}
//		}
		return null;
	}

	

}
