package com.coralogix.calculator.dto;

import java.util.Map;

import lombok.Data;
@Data
public class Fixer {
	 private String success;
	 private String timestamp;
	 private String base;
	 private String date;
	 private Map<String, Double> rates;

}
