package com.coralogix.calculator.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coralogix.calculator.model.ExchangeRate;

public interface ExchangeRateDao extends JpaRepository<ExchangeRate, Integer> {

}
