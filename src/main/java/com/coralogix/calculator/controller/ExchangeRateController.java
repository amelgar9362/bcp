package com.coralogix.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coralogix.calculator.model.ExchangeRate;
import com.coralogix.calculator.services.ExchangeRateService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("exchangerate")
public class ExchangeRateController {
	@Autowired
	ExchangeRateService exchangeRateService;
	
	@GetMapping
	Mono<ExchangeRate> getAllExchangeRate(@RequestParam String originCurrency,@RequestParam String finalCurrency){
		return exchangeRateService.getAllExchangeRate(originCurrency, finalCurrency);
		
	}

}
