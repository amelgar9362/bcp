package com.coralogix.calculator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "exchangerate")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRate {
	@Id
	private Integer id;
	@Column(name = "origin_currency")
	private String originCurrency;
	@Column(name = "final_currency")
	private String finalCurrency;
	@Column(name = "date")
	private String date;
	@Column(name = "value")
	private String value;
}
