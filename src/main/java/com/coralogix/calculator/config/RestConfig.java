package com.coralogix.calculator.config;

import java.time.Duration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {
	@Bean(name = "restTemplateClient")
	public RestTemplate restTemplateTrazabilidadRos(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.setConnectTimeout(Duration.ZERO )
				.setReadTimeout(Duration.ZERO).build();
	}

}
